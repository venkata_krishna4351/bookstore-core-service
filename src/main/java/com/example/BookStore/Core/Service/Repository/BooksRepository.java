package com.example.BookStore.Core.Service.Repository;

import com.example.BookStore.Core.Service.Model.BooksEntityClass;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface BooksRepository extends CrudRepository<BooksEntityClass, String> {

    List<BooksEntityClass> findAll();

    @Transactional
    @Query("From stock b where b.Title_name=:Title_name")
    BooksEntityClass findByTitle_name(String Title_name);

    @Transactional
    @Query("From stock b where b.Author_name=:Author_name")
    List<BooksEntityClass> findByAuthor_name(String Author_name);
}