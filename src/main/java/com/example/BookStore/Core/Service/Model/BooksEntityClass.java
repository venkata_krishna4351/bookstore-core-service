package com.example.BookStore.Core.Service.Model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table
@Entity(name = "stock")
public class BooksEntityClass {

    @Id
    @Column(name = "STOCK_ID")
    private String STOCK_ID;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "Author_name")
    private String Author_name;

    @Column(name = "Title_name")
    private String Title_name;

    @Column(name = "Publisher_name")
    private String Publisher_name;

    @Column(name = "Published_year")
    private String Published_year;

    @Column(name = "Supplier_ID")
    private String Supplier_ID;

    @Column(name = "Retail_price")
    private String Retail_price;
}
