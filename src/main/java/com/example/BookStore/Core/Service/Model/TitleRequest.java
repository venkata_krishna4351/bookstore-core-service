package com.example.BookStore.Core.Service.Model;

import lombok.Data;

@Data
public class TitleRequest {
    private String titleName;
}
