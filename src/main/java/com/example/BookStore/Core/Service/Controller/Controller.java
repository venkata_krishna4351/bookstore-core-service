package com.example.BookStore.Core.Service.Controller;


import com.example.BookStore.Core.Service.Model.AuthorRequest;
import com.example.BookStore.Core.Service.Model.BooksResponse;
import com.example.BookStore.Core.Service.Model.ListBooksResponse;
import com.example.BookStore.Core.Service.Model.TitleRequest;
import com.example.BookStore.Core.Service.ServiceImplementation.ServiceImplementation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(path = "core-service/v1/")
public class Controller {

    @Autowired
    ServiceImplementation serviceImplementation;

    @GetMapping(path = "status")
    public String getStatus() {
        return "Working";
    }

    @PostMapping(path = "get-all-books", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ListBooksResponse> getAllBooks(@RequestHeader(value = "txRoot") String txRoot,
                                                               @RequestHeader(value = "SystemId") String systemId){

        return new ResponseEntity<>(serviceImplementation.getAllBooks(txRoot, systemId), HttpStatus.FOUND);
    }

    @PostMapping(path = "book-with-title", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<BooksResponse> getBookWithTitle(@RequestHeader(value = "txRoot") String txRoot,
                                                          @RequestHeader(value = "SystemId") String systemID,
                                                          @RequestBody TitleRequest titleRequest){
        log.info( "Data : txRoot : {}, systemId : {} , bookTitle : {}", txRoot, systemID, titleRequest.getTitleName());

        return new ResponseEntity<>(serviceImplementation.getBookWithTitle(titleRequest.getTitleName(), txRoot, systemID), HttpStatus.FOUND);
    }

    @PostMapping(path = "books-with-authorname", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ListBooksResponse> getBooksWithAuthorName(@RequestHeader(value = "txRoot") String txRoot,
                                         @RequestHeader(value = "SystemId") String systemID,
                                         @RequestBody AuthorRequest authorRequest){
        log.info( "Data : txRoot : {}, systemId : {} , bookTitle : {}", txRoot, systemID, authorRequest.getAuthorName());
        return new ResponseEntity<>(serviceImplementation.getBooksWithAuthor(authorRequest.getAuthorName(), txRoot, systemID), HttpStatus.FOUND);
    }
}

