package com.example.BookStore.Core.Service.Model;

import lombok.Data;

@Data
public class BooksResponse {
    private String isbn;
    private String Author_name;
    private String Title_name;
    private String Published_year;
    private String Publisher_name;
    private String Retail_price;
}
