package com.example.BookStore.Core.Service.ServiceImplementation;

import com.example.BookStore.Core.Service.Model.BooksEntityClass;
import com.example.BookStore.Core.Service.Model.BooksResponse;
import com.example.BookStore.Core.Service.Model.ListBooksResponse;
import com.example.BookStore.Core.Service.Repository.BooksRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ServiceImplementation {

    @Autowired
    BooksRepository booksRepository;

    public ListBooksResponse getAllBooks(String txRoot, String systemId){
        ModelMapper mapper = new ModelMapper();
        log.info(" Data : TxRoot : {}, SystemId : {}", txRoot, systemId);
        List<BooksEntityClass> getAllBooksEntity = booksRepository.findAll();
        List<BooksResponse> getAllBooks = getAllBooksEntity.stream().map(e-> mapper.map(e, BooksResponse.class)).collect(Collectors.toList());
        ListBooksResponse listBooksResponse = new ListBooksResponse();
        listBooksResponse.setTxRoot(txRoot);
        listBooksResponse.setSystemId(systemId);
        listBooksResponse.setListBooksResponse(getAllBooks);
        log.info(" Data : {} ListBooksResponse :{}", getAllBooks, listBooksResponse);
        return listBooksResponse;
    }

    public BooksResponse getBookWithTitle(String title, String txRoot, String systemId){
        ModelMapper mapper = new ModelMapper();
        log.info(" Data : Title : {}, TxRoot : {}, SystemId : {}", title, txRoot, systemId);
        BooksEntityClass getBookWithTitleNameEntity = booksRepository.findByTitle_name(title);
        BooksResponse getBookWithTitleName = mapper.map(getBookWithTitleNameEntity, BooksResponse.class);
        log.info(" Data : {}", getBookWithTitleName);
        return getBookWithTitleName;
    }

    public ListBooksResponse getBooksWithAuthor(String authorName, String txRoot, String systemId){
        ModelMapper mapper = new ModelMapper();
        log.info(" Data : Author Name : {}, TxRoot : {}, SystemId : {}", authorName, txRoot, systemId);
        List<BooksEntityClass> getBooksWithAuthor= booksRepository.findByAuthor_name(authorName);
        List<BooksResponse> listGetBooksWithAuthor = getBooksWithAuthor.stream().map(e-> mapper.map(e, BooksResponse.class)).collect(Collectors.toList());
        log.info(" Data : {}", listGetBooksWithAuthor);
        ListBooksResponse listBooksResponse = new ListBooksResponse();
        listBooksResponse.setTxRoot(txRoot);
        listBooksResponse.setListBooksResponse(listGetBooksWithAuthor);
        listBooksResponse.setSystemId(systemId);
        return listBooksResponse;
    }
}
