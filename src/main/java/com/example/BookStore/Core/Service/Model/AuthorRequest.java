package com.example.BookStore.Core.Service.Model;

import lombok.Data;

@Data
public class AuthorRequest {
    private String authorName;
}
