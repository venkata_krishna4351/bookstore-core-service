package com.example.BookStore.Core.Service.Model;

import lombok.Data;

import java.util.List;

@Data
public class ListBooksResponse {
    private String txRoot;
    private List<BooksResponse> listBooksResponse;
    private String systemId;
}
